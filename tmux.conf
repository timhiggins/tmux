# Smart pane switching with awareness of Vim splits.
# See: https://github.com/christoomey/vim-tmux-navigator
is_vim="ps -o state= -o comm= -t '#{pane_tty}' \
    | grep -iqE '^[^TXZ ]+ +(\\S+\\/)?g?(view|n?vim?x?)(diff)?$'"
bind-key -n C-h if-shell "$is_vim" "send-keys C-h"  "select-pane -L"
bind-key -n C-j if-shell "$is_vim" "send-keys C-j"  "select-pane -D"
bind-key -n C-k if-shell "$is_vim" "send-keys C-k"  "select-pane -U"
bind-key -n C-l if-shell "$is_vim" "send-keys C-l"  "select-pane -R"
bind-key -n C-\ if-shell "$is_vim" "send-keys C-\\" "select-pane -l"

# Toggle mouse on with Ctrl-b m
#bind-key m \
#  set-option -g mode-mouse on \;\
#  set-option -g mouse-resize-pane on \;\
#  set-option -g mouse-select-pane on \;\
#  set-option -g mouse-select-window on \;\
#  display 'Mouse: ON'

# Toggle mouse off with Ctrl-b M
#bind-key M \
#  set-option -g mode-mouse off \;\
#  set-option -g mouse-resize-pane off \;\
#  set-option -g mouse-select-pane off \;\
#  set-option -g mouse-select-window off \;\
#  display 'Mouse: OFF'

# Enable mouse.
set-option -g -q mouse on

# Enable full color support:
set -g default-terminal "xterm"

# Highlight current window using specified color
set-window-option -g window-status-current-bg yellow

# Change prefix from Ctrl-b to Ctrl-a
unbind C-b
set -g prefix C-a

# Use arrow keys as alternate method of navigating panes
bind-key Up    select-pane -U
bind-key Down  select-pane -D
bind-key Left  select-pane -L
bind-key Right select-pane -R

# Increase the scroll-back history
set -g history-limit 30000

# Remove delay when pressing esc in Vim
set -sg escape-time 0

# Alternate clear screen mapping that doesn't conflict with custom screen navigation.
bind C-l send-keys 'C-l'

# Tell tmux to send prefix key to running program.
bind-key a send-prefix

# When entering tmux, use the terminal override option to enable true color.
#set -g default-terminal "xterm-256color"
#set-option -ga terminal-overrides ",xterm-256color:Tc"
